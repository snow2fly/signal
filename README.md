###  yongming.li Pear Limited
###  peer_id random by browser
###  peer_id is mac address by node 
### NAT Type （优先选择非SymmetricNAT/Blocked/Error类型的点，不够5个的话才用此类型的节点）
```
    * Pear_Open_FullCone        (0)
    * Pear_FullCone
    * Pear_Open_Restricted
    * Pear_Restricted
    * Pear_Open_PortRestricted
    * Pear_PortRestricted
    * Pear_Open_Symmetric
    * Pear_Symmetric
    * Pear_Blocked
    * Pear_Error
```

#### history:
#### 1.  nodes修改为数组类型，不需要多次请求，大大减少连接的时间
#### 2.  添加"nat"字段，提高连通率
#### 3.  减轻服务器的带宽压力，我们采用压缩SDP的方式。
offer_id type is : int64
``` c
1.  node ---> signal server 

{
  "action":"announce",
  "timeout":120,
  "nat":5,
  "peer_id":"mac",
  "sdps":[
   { "offer":{"type":"offer","sdp":""},"offer_id":1122334455667788},
   { "offer":{"type":"offer","sdp":""},"offer_id":1122334455667799}]
}
改为：
{
  "action":"announce",
  "timeout":120,
  "nat":5,
  "peer_id":"mac",
  "prediction_port":50000,                //新添加
  "sdps":[
   { "offer":{"type":"offer","sdp":""},"offer_id":1122334455667788,"remote_offer_id

":1122334455667788},
   { "offer":{"type":"offer","sdp":""},"offer_id":1122334455667799,"remote_offer_id

":1122334455667799}]
}
改为：
{
   "action":"announce",
   "size" :1024        //压缩数据大小。
}
紧跟在这个数据后面为压缩数据,大小由size字段给出。



(1) 添加项
request_pull信号：
2. node ---> signaling server //pull message
{
    "action" :"pull",
    "uuid":"20:76:93:3d:87:79",                 //主动发起的MAC。
    "to_uuid":"20:76:93:3d:d2:7d"               //获取这个MAC相关的信息，在信令服务器上的信息。
}

2.  signal server ---> node

{ "action":"announce","interval":120}

3.  browser ---> signal server
{
  "action":"get",
  "peer_id":"456",
  "host":"xxx.com",
  "uri":"video/pear.mp4",
  "md5":"..."
}
改为：
3(1).  browser ---> signal server
{
  "action":"get",
  "peer_id":"456",
  "host":"xxx.com",
  "uri":"video/pear.mp4",
  "to_uuid":"xx:xx:xx:xx:xx",            //在有这个字段时，就将这个UUID的SDP返回给browser，无时按之前的流程走。
  "md5":"..."
}


4. browser <--- signal server

4.1
{
   "peer_id":"mac",
   "sdp":{"type":"offer","sdp":""},
   "offer_id":1122334455667788,
   "host":"xxx.com",
   "uri":"video/pear.mp4",
   "md5":"..."
}
(1)修改为

{
  "nodes": [
		{
		  "peer_id":"mac1",
		   "sdp":{"type":"offer","sdp":""},
		   "offer_id":1122334455667788,
		   "host":"xxx.com",
		   "uri":"video/pear.mp4",
		   "md5":"..."
		},
	    {
		  "peer_id":"mac2",
		   "sdp":{"type":"offer","sdp":""},
		   "offer_id":1122334455667799,
		   "host":"xxx.com",
		   "uri":"video/pear.mp4",
		   "md5":"..."
		}
	]
}
(2)修改为
{
  "nodes": [
        {
          "peer_id":"mac1",
           "sdp":{"type":"offer","sdp":""},
           "offer_id":1122334455667788,
           "host":"xxx.com",
           "uri":"video/pear.mp4",
           "md5":"...",
            "setup":1,
            "prediction_port":50000
        }
    ]
}


4.2
{
   "errorcode":"4004",
   "msg":"No such file",
   "host":"xxx.com",
   "uri":"video/pear.mp4",
   "md5":"..."
}

4.3
signaling server ---> node
{
    "errorcode":"4004",
    "msg":"No such node",
    "uuid":"20:76:93:3d:87:79",                 //主动发起的MAC。
    "to_uuid":"20:76:93:3d:d2:7d"               //获取这个MAC相关的信息，在信令服务器上的信息。
}

4.4
signaling server ---> node
{
    "errorcode":"4004",
    "msg":"No such sdp",
    "uuid":"20:76:93:3d:87:79",                 //主动发起的MAC。
    "to_uuid":"20:76:93:3d:d2:7d"               //获取这个MAC相关的信息，在信令服务器上的信息。
}

5. browser ---> signal server  --->  node 

{
  "action":"answer",
  "peer_id":"456",
  "to_peer_id":"mac",
  "sdp":{"type":"answer","sdp":""},
  "offer_id":1122334455667788
}

6. browser ---> signal server  --->  node 
  
{
  "action":"candidate",
  "peer_id":"456",
  "to_peer_id":"70:8b:cd:a8:82:67",
  "candidate":
   { "candidate": "","sdpMid": "data","sdpMLineIndex": 0 },
  "offer_id":1122334455667788
}

// 添加： 在双向“打洞”时将被使用。
7. node ---> signal server  --->  browser
{
  "action":"candidate",
  "peer_id":"70:8b:cd:a8:82:67",
  "to_peer_id":"456",
  "candidate":
   { "candidate": "","sdpMid": "data","sdpMLineIndex": 0 },
  "offer_id":1122334455667788
}

// 添加： 确认node已经受到web端的candidate信息。
8. node ---> signal server  --->  browser
{
    "peer_id":"20:76:93:3E:64:0D",
    "to_peer_id":"456",
    "offer_id":5894460578560847,
    "action":"candidate",
    "tpye":"end"
}
改为：
8(1). node ---> signal server  --->  browser
{
    "peer_id":"20:76:93:3E:64:0D",
    "to_peer_id":"456",
    "offer_id":5894460578560847,
    "action":"candidate",
    "candidates":{
        "tpye":"end"
    }
}


// 添加： 告诉node端，browser的candidate已经全部发送完成。
9. browser ---> signal server  --->  node
{
    "peer_id":"456",
    "to_peer_id":"20:76:93:3E:64:0D",
     "offer_id":5894460578560847,
     "action":"candidate",
     "candidates":{
		"candidate":"completed"
      }
}

// 添加： 告诉node端，立即生成sdp。
// 这个命令，最好考虑一下超时的问题。
10. signal server  --->  node
{
    "action":"generate"
}


// 添加： 只返回对应的MAC地址。
11. node ----->  signal server
{
    "action":"get_mac",
    "peer_id":"456",
    "host":"xxx.com",
    "nat":xxx,
    "uri":"video/pear.mp4",
    "md5":"..."
}

//get_mac命令的返回。
11(1). signal server ----->  node
{
"nodes":[
    {"peer_id":"xx:xx:xx:xx:xx:xx"},
    {"peer_id":"xx:xx:xx:xx:xx:xx"},
    {"peer_id":"xx:xx:xx:xx:xx:xx"}
   ]
    "errorcode":"xxxxxx",
    "msg":"xxxxx"
}
```








