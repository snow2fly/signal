package register

import (
	"crypto/md5"
	"encoding/json"
	//"errors"
	"fmt"
	"github.com/mohong122/ip2region/binding/golang"
	"github.com/sirupsen/logrus"
	"github.com/valyala/fasthttp"
	"log"
	"math/rand"
	"strings"
	"time"
)

type BrowserGet struct {
	Action string `json:"action"`
	PeerId string `json:"peer_id"`
	Host   string `json:"host"`
	Uri    string `json:"uri"`
	Md5    string `json:"md5"`
	NeedMacs []string `json:"need_macs"`
	NeedNum int `json:"need_num"`//需要的节点数
	VideoID string `json:"video_id"`//即B站视频的cid B站专用
	Blacklist []string `json:"blacklist"`//尽量不要返回这些mac
	//ToUuid  string `json:"to_uuid"`
}

type Scheduler struct {
	Nodes []*MacAddr `json:"nodes"`
}
type MacAddr struct {
	Mac string `json:"mac"`
}

type NodeAddress struct {
	MacAddr   string
	Scheme    string
	PublicIp  string
	HttpPort  int32
	HttpsPort int32
}

type NodeStates struct {
	nodeAddress []*NodeAddress
}

type BrowserGetResponse struct {
	Nodes []*BrowserGetNodeInfo `json:"nodes"`
	Action       string     `json:"action,omitempty"`
	Host       string     `json:"host"`
	Uri       string     `json:"uri"`
	ErrorCode string     `json:"errorcode,omitempty"`
	Msg       string     `json:"msg,omitempty"`
}

type BrowserGetNodeInfo struct {
	PeerId    string    `json:"peer_id"`
	Sdp       NodeOffer `json:"sdp"`
	OfferId   int64     `json:"offer_id"`
	Host      string    `json:"host"`
	Uri       string    `json:"uri"`
	Md5       string    `json:"md5"`
	ErrorCode string    `json:"errorcode"`
	Msg       string    `json:"msg"`
	Nat       int64     `json:"nat"`
}

type RegionDecoder interface {
	Region(string) (ip2region.IpInfo, error)
}

type GetHandler struct {
	client        *Client
	remoteAddr    string
	regionDecoder RegionDecoder
}
type NoInLocal struct {
	Mac []*string `json:"noInLocal"`
}

//func (g *GetHandler) Handle(message []byte) {
//	var browserGet BrowserGet
//	if err := json.Unmarshal(message, &browserGet); err != nil {
//		log.Printf("[error]:get handler json unmarshal err:%s", err.Error())
//		//TODO 错误处理
//		return
//	}
//	peerId := strings.ToLower(browserGet.PeerId)
//	g.client.lock.Lock()
//	defer g.client.lock.Unlock()
//	_, ok := g.client.server.clients[peerId]
//
//	if !ok {
//		localHost := g.client.server.localHost
//		node := *g.client.nodeAnnounce
//		node.PeerId = peerId
//		node.Time = g.client.GetTime()
//		node.Timeout = 120
//		node.PeerId = peerId
//		go func(a NodeAnnounce, host string) {
//			g.client.register = true
//			log.Printf("announce register to station:%v", a.Time)
//			const baseUrl = "register?node=%s&server=%s"
//			fastClient := &fasthttp.Client{
//				//ReadTimeout: 5*time.Second,
//			}
//			body, err := json.Marshal(node)
//			if err != nil {
//				log.Printf("[error]:set node announce json encode err:%v", err)
//				return
//			}
//			log.Printf("announce body:%v", string(body))
//			req := fasthttp.AcquireRequest()
//			req.Header.SetMethod("POST")
//			req.SetRequestURI(fmt.Sprintf(baseUrl, node.PeerId, localHost))
//			req.SetHost(g.client.server.remoteHost)
//			req.SetBody(body)
//			resp := fasthttp.AcquireResponse()
//			if err := fastClient.DoTimeout(req, resp, 10*time.Second); err != nil {
//				log.Printf("[err]:client send announce to transform server->%v", err)
//
//			}
//		}(node, localHost)
//
//		g.client.server.clients[peerId] = *g.client
//
//	}
//	//for mac, node := range g.client.server.clients {
//	//	m := strings.ToLower(mac)
//	//	log.Printf("[info]: node scheduler mac:%s", m)
//	//	nodeInfo, _, err := g.getNodeInfo(node.nodeAnnounce, browserGet.Host, browserGet.Uri, browserGet.Md5)
//	//	if err != nil {
//	//		log.Printf("[error]: get in local node info err:%v", err)
//	//	}
//	//	result := BrowserGetResponse{}
//	//	result.Nodes = append(result.Nodes, nodeInfo)
//	//	result.Nodes = append(result.Nodes, nodeInfo)
//	//	g.client.jsonResponse(result)
//	//	log.Printf("get response to browser:%v", *result.Nodes[0])
//	//	break
//	//}
//
//	g.client.SetId(browserGet.PeerId)
//	go func() {
//		log.Printf("push to register server")
//		const baseUrl = "v1/customer/nodes/unhealth?client_ip=%s&host=%s&uri=%s"
//		fastClient := &fasthttp.Client{
//			//ReadTimeout: 5*time.Second,
//		}
//		req := fasthttp.AcquireRequest()
//		req.Header.SetMethod("GET")
//		req.SetRequestURI(fmt.Sprintf(baseUrl, g.client.RemoteAddr(), browserGet.Host, browserGet.Uri))
//		log.Printf("[info]: base url%v", fmt.Sprintf(baseUrl, g.client.RemoteAddr(), browserGet.Host, browserGet.Uri))
//		req.SetHost(g.client.server.scheduler)
//		resp := fasthttp.AcquireResponse()
//		if err := fastClient.DoTimeout(req, resp, 10*time.Second); err != nil {
//			log.Printf("node push to server:%s ", err)
//		}
//		if resp.StatusCode() != fasthttp.StatusOK {
//			log.Printf("[error]:get handler get node info err:%d", resp.StatusCode())
//			return
//		}
//		scheduler := new(Scheduler)
//		nodeStates := new(NodeStates)
//		if len(resp.Body()) != 0 {
//			log.Printf("[info]:http get scheduler resp body:%v", string(resp.Body()))
//			err := json.Unmarshal(resp.Body(), scheduler)
//			if err != nil {
//				log.Printf("[error]:get handle unhealth node json unmarshal err:%v", err)
//			}
//			for _, s := range scheduler.Nodes {
//				var nodeAddr NodeAddress
//				nodeAddr.MacAddr = s.Mac
//				nodeStates.nodeAddress = append(nodeStates.nodeAddress, &nodeAddr)
//			}
//			log.Printf("[info]:http get scheduler request:%v", nodeStates.nodeAddress[0].MacAddr)
//
//			if g.responseVideo(browserGet, nodeStates, g.client.RemoteAddr()) {
//				return
//			}
//		}
//		g.responseNoVideo(browserGet)
//		//		TODO
//	}()
//
//}

func (g *GetHandler) Handle(message []byte) {
	var browserGet BrowserGet
	
	
	if err := json.Unmarshal(message, &browserGet); err != nil {
		log.Printf("[error]:get handler json unmarshal err:%s", err.Error())
		//TODO 错误处理
		return
	}
	log.Printf("[info] 777777777777777777777777777777777777777777777777777777777777777777777777")
	peerId := strings.ToLower(browserGet.PeerId)
	g.client.server.lock.Lock()
	_, ok := g.client.server.clients[peerId]

	blacklistMap := make(map[string]bool)
	for _, mac := range browserGet.Blacklist {
		blacklistMap[mac] = true
	}

	needNum := 10
	if browserGet.NeedNum > 0 {
		needNum = browserGet.NeedNum
	}

	if !ok {
		log.Printf("[info] aaa peerId not in clients")
		localHost := g.client.server.localHost
		node := g.client.nodeAnnounce
		node.PeerId = peerId
		node.Time = g.client.GetTime()
		node.Timeout = 120
		node.PeerId = peerId
		go func(a NodeAnnounce, host string) {
			log.Printf("[info] aaa in go func")
			g.client.register = true
			log.Printf("announce register to station:%v", a.Time)
			const baseUrl = "register?node=%s&server=%s"
			fastClient := &fasthttp.Client{
				//ReadTimeout: 5*time.Second,
			}
			body, err := json.Marshal(node)
			if err != nil {
				log.Printf("[error]:set node announce json encode err:%v", err)
				return
			}
			log.Printf("get body:%v", string(body))
			req := fasthttp.AcquireRequest()
			req.Header.SetMethod("POST")
			req.SetRequestURI(fmt.Sprintf(baseUrl, node.PeerId, localHost))
			req.SetHost(g.client.server.remoteHost)
			req.SetBody(body)
			resp := fasthttp.AcquireResponse()
			if err := fastClient.DoTimeout(req, resp, 10*time.Second); err != nil {
				log.Printf("[err]:(3)client send announce to transform server->%v", err)

			}
		}(node, localHost)

		g.client.server.clients[peerId] = *g.client

	}
	g.client.server.lock.Unlock()
	//for mac, node := range g.client.server.clients {
	//	m := strings.ToLower(mac)
	//	log.Printf("[info]: node scheduler mac:%s", m)
	//	nodeInfo, _, err := g.getNodeInfo(node.nodeAnnounce, browserGet.Host, browserGet.Uri, browserGet.Md5)
	//	if err != nil {
	//		log.Printf("[error]: get in local node info err:%v", err)
	//	}
	//	result := BrowserGetResponse{}
	//	result.Nodes = append(result.Nodes, nodeInfo)
	//	result.Nodes = append(result.Nodes, nodeInfo)
	//	g.client.jsonResponse(result)
	//	log.Printf("get response to browser:%v", *result.Nodes[0])
	//	break
	//}
	//

	//if err := json.Unmarshal(message, &browserGet); err != nil {
	//	log.Printf("[error]:get handler json unmarshal err:%s", err.Error())
	//	//TODO 错误处理
	//	return
	//}

	g.client.server.lock.Lock()
	nodeStates := new(NodeStates)
	var  i int
	var nodesNotMatch []*NodeAddress
	var nodesInBlacklist []*NodeAddress
	useMatch := false
	hashID := byte(0)
	if browserGet.VideoID != "" {
		useMatch = true
		hashID = md5.Sum([]byte(browserGet.VideoID))[15] & byte(15)
	}
	for m, node := range g.client.server.clients {
		if node.nodeAnnounce.Nat <= 5 && !strings.Contains(m, "-") {
			if useMatch {
				nodeHashID := md5.Sum([]byte(m))[15] & byte(15)
				if hashID != nodeHashID {
					nodesNotMatch = append(nodesNotMatch, &NodeAddress{MacAddr: m})
					continue
				}
			}
			if blacklistMap[m] {
				nodesInBlacklist = append(nodesInBlacklist, &NodeAddress{MacAddr: m})
				continue
			}
			var nodeAddr NodeAddress
			nodeAddr.MacAddr = m
			nodeStates.nodeAddress = append(nodeStates.nodeAddress, &nodeAddr)
			i++
			if i >= needNum{
				break
			}
		}
	}

	if i < needNum && len(nodesNotMatch) > 0 {
		for j := 0; j < len(nodesNotMatch); j ++ {
			nodeStates.nodeAddress = append(nodeStates.nodeAddress, nodesNotMatch[j])
			i++
			if i >= needNum {
				break
			}
		}
	}

	if i < needNum && len(nodesInBlacklist) > 0 {
		for j := 0; j < len(nodesInBlacklist); j ++ {
			nodeStates.nodeAddress = append(nodeStates.nodeAddress, nodesInBlacklist[j])
			i++
			if i >= needNum {
				break
			}
		}
	}

	//______________________________________________________________________________________
	//00:1c:42:0b:de:24
	//00:0c:29:62:ad:55
	//var nodeAddr0 NodeAddress
	//nodeAddr0.MacAddr = "20:76:93:93:93:93"
	//nodeStates.nodeAddress = append(nodeStates.nodeAddress, &nodeAddr0)
	//var nodeAddr1 NodeAddress
	//nodeAddr1.MacAddr = "21:26:fb:bc:a7:11"
	//nodeStates.nodeAddress = append(nodeStates.nodeAddress, &nodeAddr1)
	//var nodeAddr2 NodeAddress
	//nodeAddr2.MacAddr = "20:76:93:4b:23:db"
	//nodeStates.nodeAddress = append(nodeStates.nodeAddress, &nodeAddr2)
	//var nodeAddr3 NodeAddress
	//nodeAddr3.MacAddr = "70:8b:cd:a8:82:67"
	//nodeStates.nodeAddress = append(nodeStates.nodeAddress, &nodeAddr3)
	//var nodeAddr4 NodeAddress
	//nodeAddr4.MacAddr = "00:e0:70:56:08:0f"
	//nodeStates.nodeAddress = append(nodeStates.nodeAddress, &nodeAddr4)

	if browserGet.NeedMacs != nil {
		for _, mac := range browserGet.NeedMacs {
			nodeStates.nodeAddress = append(nodeStates.nodeAddress, &NodeAddress{ MacAddr: strings.ToLower(mac)})
		}
	}
	//______________________________________________________________________________________

	remoteAddr := g.remoteAddr
	g.client.server.lock.Unlock()

	if g.responseVideo(browserGet, nodeStates,  remoteAddr) {
		return
	}
}

func (g *GetHandler) responseVideo(request BrowserGet, nodes *NodeStates, remoteIp string) bool {
	//isOk := false
	log.Printf("[info]: responseVideo remoteIp:%v", remoteIp)
	//less than 5 return all nodes
	//if len(nodes) > 5 {
	//	nodes.RegionFilter(remoteIp, g.regionDecoder, func(states NodeStates) bool {
	//		if g.nodeSchedule(request, states) {
	//			isOk = true
	//			return false
	//		}
	//		return true
	//	})
	//	if isOk {
	//		return true
	//	}
	//}
	// TODO 重复过滤
	return g.nodeSchedule(request, nodes)

}

//func (g *GetHandler) nodeSchedule(request BrowserGet, nodes *NodeStates) bool {
//	logrus.Debugf("[GetHandler.nodeSchedule] into len(%d)", len(nodes.nodeAddress))
//	result := BrowserGetResponse{}
//	//var natFilterNodes []*BrowserGetNodeInfo
//	noInLocal := new(Scheduler)
//	if len(nodes.nodeAddress) == 0 {
//		return false
//	}
//	//过滤网络类型
//	for _, node := range nodes.nodeAddress {
//		mac := strings.ToLower(node.MacAddr)
//		log.Printf("[info]: node scheduler mac:%s", mac)
//		g.client.lock.Lock()
//		peer, ok := g.client.server.clients[mac]
//		g.client.lock.Unlock()
//		if !ok {
//			noInLocal.Nodes = append(noInLocal.Nodes, &MacAddr{Mac: mac})
//			continue
//		}
//		nodeInfo, _, err := g.getNodeInfo(peer.nodeAnnounce, request.Host, request.Uri, request.Md5)
//		if err != nil {
//			log.Printf("[error]: get in local node info err:%v", err)
//			continue
//		}
//
//		//nodeInfo, natFilteFlag, err := g.getNodeInfo(peer.nodeAnnounce, request.Host, request.Uri, request.Md5)
//		//if err != nil {
//		//	log.Printf("[error]: get in local node info err:%v", err)
//		//	continue
//		//}
//		//if !natFilteFlag {
//		//	result.Nodes = append(result.Nodes, nodeInfo)
//		//} else {
//		//	natFilterNodes = append(natFilterNodes, nodeInfo)
//		//}
//
//		result.Nodes = append(result.Nodes, nodeInfo)
//
//	}
//	if len(noInLocal.Nodes) != 0 {
//		log.Printf("push to register server body: %v", noInLocal)
//		const baseUrl = "get"
//		fastClient := &fasthttp.Client{
//			//ReadTimeout: 5*time.Second,
//		}
//		body, err := json.Marshal(noInLocal)
//		log.Printf("[info]: get no in local node:%v", string(body))
//		if err != nil {
//			log.Printf("[error]: get not in local json marshal err:%v", err)
//			goto next
//		}
//		req := fasthttp.AcquireRequest()
//		req.Header.SetMethod("POST")
//		req.SetRequestURI(baseUrl)
//		req.SetHost(g.client.server.remoteHost)
//		req.SetBody(body)
//		resp := fasthttp.AcquireResponse()
//		if err := fastClient.DoTimeout(req, resp, 10*time.Second); err != nil {
//			log.Printf("node push to server:%s ", err)
//		}
//		if resp.StatusCode() != fasthttp.StatusOK {
//			log.Printf("[error]: get not in local info err:%d", resp.StatusCode())
//			goto next
//		}
//		log.Printf("[info]: node get in station 1:%v", len(resp.Body()))
//		if len(resp.Body()) != 0 {
//			var getNode []NodeAnnounce
//			log.Printf("[info]: node get in station 2:%v", string(resp.Body()))
//			err = json.Unmarshal(resp.Body(), &getNode)
//			if err != nil {
//				log.Printf("[error]: get not in local resp json unmarshal err:%v", err)
//				goto next
//			}
//			//for _, peer := range getNode {
//			//	nodeInfo, natFilteFlag, err := g.getNodeInfo(&peer, request.Host, request.Uri, request.Md5)
//			//	if err != nil {
//			//		log.Printf("[error]: get not in local node info err:%v", err)
//			//		continue
//			//	}
//			//	if !natFilteFlag {
//			//		result.Nodes = append(result.Nodes, nodeInfo)
//			//	} else {
//			//		natFilterNodes = append(natFilterNodes, nodeInfo)
//			//	}
//			//}
//			for _, peer := range getNode {
//				nodeInfo, _, err := g.getNodeInfo(&peer, request.Host, request.Uri, request.Md5)
//				if err != nil {
//					log.Printf("[error]: get not in local node info err:%v", err)
//					continue
//				}
//				result.Nodes = append(result.Nodes, nodeInfo)
//			}
//			g.client.jsonResponse(result)
//		}
//	}
//	g.client.jsonResponse(result)
//
//next:
//	if len(result.Nodes) < 5 {
//		//if len(result.Nodes) == 0 && len(natFilterNodes) == 0 {
//		//	return false
//		//}
//		//result.Nodes = append(result.Nodes, natFilterNodes...)
//		//if len(result.Nodes) < 5 {
//		//	g.client.jsonResponse(result)
//		//} else {
//		//	result.Nodes = result.Nodes[:5]
//		//	g.client.jsonResponse(result)
//		//}
//		log.Printf("[info] get next")
//		return true
//
//	} else {
//		result.Nodes = g.getRandNode(result.Nodes)
//		g.client.jsonResponse(result)
//		return true
//	}
//
//	return false
//}

func (g *GetHandler) nodeSchedule(request BrowserGet, nodes *NodeStates) bool {
	logrus.Debugf("[GetHandler.nodeSchedule] into len(%d)", len(nodes.nodeAddress))
	result := BrowserGetResponse{}
	//result := MacNodeInfo{}
	//result.ErrorCode = "2000"
	//result.Msg = "mac has been send"
	result.Action = "nodes_mac"
	result.Host = request.Host
	result.Uri = request.Uri

	//noInLocal := new(Scheduler)
	if len(nodes.nodeAddress) == 0 {
		return false
	}
	//过滤网络类型
	for _, node := range nodes.nodeAddress {
		mac := strings.ToLower(node.MacAddr)
		log.Printf("[info]: 0000000000000000000000000000000    node scheduler mac:%s", mac)
		g.client.server.lock.Lock()
		peer, _ := g.client.server.clients[mac]
		g.client.server.lock.Unlock()
		//if !ok {
		//	noInLocal.Nodes = append(noInLocal.Nodes, &MacAddr{Mac: mac})
		//	continue
		//}
		nodeInfo, _, err := g.getNodeInfo(&peer.nodeAnnounce, request.Host, request.Uri, request.Md5)
		if err != nil {
			log.Printf("[error]: get in local node info err:%v", err)
			continue
		}
		//
		//nodeInfo, _, err = g.getNodeInfo(&peer.nodeAnnounce, request.Host, request.Uri, request.Md5)
		//if err != nil {
		//	log.Printf("[error]: get in local node info err:%v", err)
		//	continue
		//}
		if nodeInfo.PeerId == "" {
			nodeInfo.PeerId = mac
		}

		result.Nodes = append(result.Nodes, nodeInfo)
		//result.Mac = append(result.Mac, nodesMac{PeerId: mac})
	}

	/*if len(noInLocal.Nodes) != 0 {
		log.Printf("push to register server body: %v", noInLocal)
		const baseUrl = "get"
		fastClient := &fasthttp.Client{
			//ReadTimeout: 5*time.Second,
		}
		body, err := json.Marshal(noInLocal)
		log.Printf("[info]: get no in local node:%v", string(body))
		if err != nil {
			log.Printf("[error]: get not in local json marshal err:%v", err)
			goto next
		}
		req := fasthttp.AcquireRequest()
		req.Header.SetMethod("POST")
		req.SetRequestURI(baseUrl)
		req.SetHost(g.client.server.remoteHost)
		req.SetBody(body)
		resp := fasthttp.AcquireResponse()
		if err := fastClient.DoTimeout(req, resp, 10*time.Second); err != nil {
			log.Printf("node push to server:%s ", err)
		}
		if resp.StatusCode() != fasthttp.StatusOK {
			log.Printf("[error]: get not in local info err:%d", resp.StatusCode())
			goto next
		}
		log.Printf("[info]: node get in station 1:%v", len(resp.Body()))
		if len(resp.Body()) != 0 {
			var getNode []NodeAnnounce
			log.Printf("[info]: node get in station 2:%v", string(resp.Body()))
			err = json.Unmarshal(resp.Body(), &getNode)
			if err != nil {
				log.Printf("[error]: get not in local resp json unmarshal err:%v", err)
				goto next
			}
			//for _, peer := range getNode {
			//	nodeInfo, natFilteFlag, err := g.getNodeInfo(&peer, request.Host, request.Uri, request.Md5)
			//	if err != nil {
			//		log.Printf("[error]: get not in local node info err:%v", err)
			//		continue
			//	}
			//	if !natFilteFlag {
			//		result.Nodes = append(result.Nodes, nodeInfo)
			//	} else {
			//		natFilterNodes = append(natFilterNodes, nodeInfo)
			//	}
			//}
			for _, peer := range getNode {
				nodeInfo, _, err := g.getNodeInfo(&peer, request.Host, request.Uri, request.Md5)
				if err != nil {
					log.Printf("[error]: get not in local node info err:%v", err)
					continue
				}
				result.Nodes = append(result.Nodes, nodeInfo)
			}
			g.client.jsonResponse(result)
		}
	}*/
	log.Printf("get response rsult:%+v",result)
	g.client.jsonResponse(result)
	return false

/*next:
	if len(result.Nodes) < 5 {
		//if len(result.Nodes) == 0 && len(natFilterNodes) == 0 {
		//	return false
		//}
		//result.Nodes = append(result.Nodes, natFilterNodes...)
		//if len(result.Nodes) < 5 {
		//	g.client.jsonResponse(result)
		//} else {
		//	result.Nodes = result.Nodes[:5]
		//	g.client.jsonResponse(result)
		//}
		log.Printf("[info] get next")
		return true

	} else {
		result.Nodes = g.getRandNode(result.Nodes)
		g.client.jsonResponse(result)
		return true
	}*/


}

func (g *GetHandler) getRandNode(result []*BrowserGetNodeInfo) (response []*BrowserGetNodeInfo) {

	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	randRecord := make(map[int]int)
	nodesSize := len(result)

	for i := 0; i < nodesSize && len(response) <= 5; i++ {
		index := r.Intn(nodesSize)
		if _, exist := randRecord[index]; exist {
			continue
		}
		randRecord[index] = 1

		response = append(response, result[index])
	}
	return response
}

func (g *GetHandler) getNodeInfo(n *NodeAnnounce, Host, Uri, Md5 string) (*BrowserGetNodeInfo, bool, error) {
	natFilteFlag := false
	nodeInfo := BrowserGetNodeInfo{}

	/*if len(n.Sdps) < 1 {
		return &nodeInfo, natFilteFlag, errors.New(
			fmt.Sprintf("[GetHandler.getNodeInfo]:Sdps.len < 1 "))
	}
	logrus.Debugf("%v ", g.client.server.clients)

	if time.Now().Unix()-n.Time > n.Timeout {
		return &nodeInfo, natFilteFlag,
			errors.New(fmt.Sprintf("[GetHandler.getNodeInfo]: timeout"))
	}*/

	if n.Nat >= 4 {
		natFilteFlag = true
	}

	//nodeInfo = BrowserGetNodeInfo{
	//	Host: Host,
	//	Uri:  Uri,
	//	Md5:  Md5,
	//}
	nodeInfo.Host = Host
	nodeInfo.Uri = Uri
	nodeInfo.Md5 = Md5
	nodeInfo.Nat = n.Nat
	nodeInfo.PeerId = n.PeerId
	if len(n.Sdps) > 0 {
		nodeInfo.Sdp = NodeOffer{
			Sdp:  n.Sdps[0].Offer.Sdp,
			Type: n.Sdps[0].Offer.Type,
		}
		nodeInfo.OfferId = n.Sdps[0].OfferId

		n.Sdps = n.Sdps[1:]
	}

	log.Printf("[getNodeInfo] force return: %+v", nodeInfo)
	return &nodeInfo, natFilteFlag, nil
}

func (g *GetHandler) responseNoVideo(request BrowserGet) {
	nodeInfo := &BrowserGetNodeInfo{
		Host:      request.Host,
		Uri:       request.Uri,
		Md5:       request.Md5,
		ErrorCode: "4004",
		Msg:       "No such file",
	}
	result := BrowserGetResponse{}
	result.Nodes = append(result.Nodes, nodeInfo)
	log.Printf("[GetHandler.responseNoVideo] no such file: %s %s", request.Host, request.Uri)
	g.client.jsonResponse(result)
}


