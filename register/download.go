package register

import (
	//"gitee.com/FogVDN-API/pear/api/mixin"
	//"gitee.com/FogVDN-API/pear/api/model"
	"gitee.com/FogVDN-API/pear/rpc/client/download"
)

type DownloadClient struct {
	client *download.DownloadClient
}

func NewDownloadClient(client *download.DownloadClient) *DownloadClient {
	return &DownloadClient{
		client: client,
	}
}

/*func (d *DownloadClient) DownloadCpVideo(video model.VideoId) mixin.ErrorCode {
	// TODO 是否达到触发下载条件
	return d.client.DownloadCpVideo(video)
}*/
