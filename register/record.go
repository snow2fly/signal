package register

//
//import (
//	"fmt"
//	"strings"
//	"time"
//
//	"github.com/sirupsen/logrus"
//
//	"github.com/xyproto/simpleredis"
//)
//
//const dbIndex = 2
//
//var addRecordCh = make(chan *NodeAnnounce)
//var delRecordCh = make(chan string)
//var chExit = make(chan int)
//
//type NodeRecord struct {
//	Pool    *simpleredis.ConnectionPool
//	NodeMap *simpleredis.HashMap
//	SizeKV  *simpleredis.KeyValue
//}
//
//func NewNodeRecord(pool *simpleredis.ConnectionPool) NodeRecord {
//	nodeMap := simpleredis.NewHashMap(pool, "nodeinfo")
//	nodeMap.SelectDatabase(dbIndex)
//
//	sizeKV := simpleredis.NewKeyValue(pool, "nodeinfo")
//	nodeMap.SelectDatabase(dbIndex)
//
//	return NodeRecord{
//		Pool:    pool,
//		NodeMap: nodeMap,
//		SizeKV:  sizeKV,
//	}
//}
//
//var NatType = []string{
//	0: "OpenInternet",
//	1: "FullCone",
//	2: "RestricNAT",
//	3: "RestricPortNAT",
//	4: "SymmetricNAT",
//	5: "Blocked/Error",
//}
//
//func (r *NodeRecord) NodeInfoRecord() {
//
//	r.NodeMap.Clear()
//	r.SizeKV.Clear()
//
//	go r.addNodeInfo()
//	go r.delNodeInfo()
//
//	select {
//	case <-chExit:
//		r.NodeMap.Clear()
//		r.SizeKV.Clear()
//
//	}
//}
//
//func (r *NodeRecord) addNodeInfo() {
//
//	var nodeinfo *NodeAnnounce
//	for {
//		select {
//		case nodeinfo = <-addRecordCh:
//			logrus.Debugf("add node")
//			macLower := strings.ToLower(nodeinfo.PeerId)
//			exist, err := r.NodeMap.Exists(macLower)
//			if !exist {
//				_, err = r.Pool.Get(dbIndex).Do("INCR", "nodeinfo:nodesize")
//				if err != nil {
//					logrus.Error(err.Error())
//				}
//			}
//
//			err = r.NodeMap.Set(macLower, "nat", NatType[nodeinfo.Nat])
//			if err != nil {
//				logrus.Error(err.Error())
//			}
//
//			err = r.NodeMap.Set(macLower, "Sdps", fmt.Sprintf("%v", nodeinfo.Sdps))
//			if err != nil {
//				logrus.Error(err.Error())
//			}
//			err = r.NodeMap.Set(macLower, "time", time.Now().Format("2006-01-02 15:04:05"))
//			if err != nil {
//				logrus.Error(err.Error())
//			}
//
//		}
//	}
//}
//
//func (r *NodeRecord) delNodeInfo() {
//
//	var clientID string
//
//	for {
//		select {
//		case clientID = <-delRecordCh:
//
//			macLower := strings.ToLower(clientID)
//			exist, err := r.NodeMap.Exists(macLower)
//			if exist {
//				logrus.Debugf("del node:%s", macLower)
//				err = r.NodeMap.Del(macLower)
//				if err != nil {
//					logrus.Error(err.Error())
//				}
//				_, err = r.Pool.Get(dbIndex).Do("DECR", "nodeinfo:nodesize")
//				if err != nil {
//					logrus.Error(err.Error())
//				}
//			}
//		}
//	}
//}
