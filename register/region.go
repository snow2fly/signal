package register

import (
	"github.com/mohong122/ip2region/binding/golang"
	"github.com/sirupsen/logrus"
	"log"
)

type IpRegionDecoder struct {
	region *ip2region.Ip2Region
}

func NewRegionDecoder(region *ip2region.Ip2Region) RegionDecoder {
	return &IpRegionDecoder{region}
}

func (i *IpRegionDecoder) Region(ip string) (ip2region.IpInfo, error) {
	log.Printf("[info]: ip region decoder ip:%s", ip)
	regionIp, err := i.region.MemorySearch(ip)
	if err != nil {
		logrus.Debugf("[error]:ip:%s region decoder memory search err:%v", ip, err)
		return ip2region.IpInfo{}, err
	}
	return regionIp, nil
}
