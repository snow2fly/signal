package register

//
//import (
//	"math/rand"
//
//	"github.com/sirupsen/logrus"
//
//	"github.com/mohong122/ip2region/binding/golang"
//)
//
//const RegionFilterSize = 10
//
//func (n NodeStates) RegionFilter(ip string,  regionDecoder RegionDecoder, receiveStatesFun func(NodeStates) bool) {
//
//	regionInfo, err := regionDecoder.Region(ip)
//	if err != nil {
//		logrus.Errorf("[NodeStates.RegionFilter] regionDecoder.Region(%s) err: %s", ip, err.Error())
//		receiveStatesFun(n)
//		return
//	}
//	logrus.Debugf("[NodeStates.RegionFilter] %s: %s, %s, %s, %s", ip, regionInfo.ISP, regionInfo.Province, regionInfo.Region, regionInfo.Country)
//
//	n.filters(RegionFilters(regionInfo), receiveStatesFun)
//}
//
//func (n NodeStates) filters(filters []RegionFilterFun, receiveStatesFun func(NodeStates) bool) {
//	var nodes NodeStates
//
//	filterLen := len(filters)
//
//	for i, filter := range filters {
//		nodes = append(nodes, n.filter(filter)...)
//		if len(nodes) < RegionFilterSize && i != filterLen-1 {
//			continue
//		}
//		if !receiveStatesFun(nodes.shuffle()) {
//			return
//		}
//	}
//}
//
//func (n NodeStates) filter(filter RegionFilterFun) (result NodeStates) {
//	for _, v := range n {
//		if filter(v.Region) {
//			result = append(result, v)
//		}
//	}
//	return
//}
//
//func (n NodeStates) shuffle() NodeStates {
//	shuffled := make(NodeStates, len(n))
//	for i, j := range rand.Perm(len(n)) {
//		shuffled[j] = n[i]
//	}
//	return shuffled
//}
//
//type RegionFilterFun func(info *ip2region.IpInfo) bool
//
//func RegionFilters(r ip2region.IpInfo) (result []RegionFilterFun) {
//	result = []RegionFilterFun{
//		func(region *ip2region.IpInfo) bool {
//			return (region.ISP == r.ISP) && (region.Province == r.Province)
//		},
//		func(region *ip2region.IpInfo) bool {
//			return (region.ISP == r.ISP) && (region.Province != r.Province) && (region.Region == r.Region)
//		},
//		func(region *ip2region.IpInfo) bool {
//			return (region.ISP == r.ISP) && (region.Province != r.Province) && (region.Region != r.Region) && (region.Country == r.Country)
//		},
//		func(region *ip2region.IpInfo) bool {
//			return (region.ISP != r.ISP) && (region.Province == r.Province)
//		},
//		func(region *ip2region.IpInfo) bool {
//			return (region.ISP != r.ISP) && (region.Province != r.Province) && (region.Region == r.Region)
//		},
//		func(region *ip2region.IpInfo) bool {
//			return (region.ISP != r.ISP) && (region.Province != r.Province) && (region.Region != r.Region) && (region.Country == r.Country)
//		},
//	}
//	return
//}
